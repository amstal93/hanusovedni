# Generated by Django 3.2.8 on 2021-11-26 12:32

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ("home", "0111_auto_20211022_0854"),
    ]

    operations = [
        migrations.AddField(
            model_name="event",
            name="buttons",
            field=wagtail.core.fields.StreamField(
                [
                    (
                        "button",
                        wagtail.core.blocks.StructBlock(
                            [
                                ("url", wagtail.core.blocks.URLBlock()),
                                ("sk_text", wagtail.core.blocks.CharBlock()),
                                ("en_text", wagtail.core.blocks.CharBlock()),
                            ]
                        ),
                    )
                ],
                blank=True,
                help_text="Tlačidlá len pre toto podujatie. Zobrazia sa vedla tlačidiel pre lístky.",
            ),
        ),
    ]
